package com.duoc.isabelazocar_prueba1.Clases;

/**
 * Created by DUOC on 22-04-2017.
 */

public class Usuario {
    private String nombre;
    private String clave;

    public Usuario(String nombre, String clave) {
        setNombre(nombre);
        setClave(clave);
    }

    public Usuario(String nombre) {
        setNombre(nombre);
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
