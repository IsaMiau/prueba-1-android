package com.duoc.isabelazocar_prueba1.Clases;

import java.util.ArrayList;

/**
 * Created by DUOC on 22-04-2017.
 */

public class BaseDatos {
    private static ArrayList<Usuario> usuarios = new ArrayList<>();
    public static void agregarUsuario(Usuario usuario){
        usuarios.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return usuarios;
    }
}
