package com.duoc.isabelazocar_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.duoc.isabelazocar_prueba1.Clases.BaseDatos;
import com.duoc.isabelazocar_prueba1.Clases.Usuario;

import java.util.ArrayList;

public class ListadoUsuarioActivity extends AppCompatActivity {

    private ListView lvLista;
    private ArrayList<Usuario> dataSource;
    private Button btnSalir;
    private static ArrayList<Usuario> u = BaseDatos.obtieneListadoUsuarios();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);

        lvLista = (ListView)findViewById(R.id.lvLista);
        AdaptadorActivity adaptador = new AdaptadorActivity(this, getDataSource());
        lvLista.setAdapter(adaptador);

        lvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListadoUsuarioActivity.this,
                        getDataSource().get(position).getNombre(), Toast.LENGTH_LONG).show();
            }
        });


        btnSalir = (Button)findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListadoUsuarioActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public ArrayList<Usuario> getDataSource() {

        if(dataSource == null){
            dataSource = new ArrayList<>();

            for(int x = 0; x < u.size(); x++){
                Usuario user = new Usuario(u.get(x).getNombre());
                dataSource.add(user);
            }
        }

        return dataSource;
    }
}
