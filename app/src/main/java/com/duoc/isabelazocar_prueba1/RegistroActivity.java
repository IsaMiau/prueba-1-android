package com.duoc.isabelazocar_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.duoc.isabelazocar_prueba1.Clases.BaseDatos;
import com.duoc.isabelazocar_prueba1.Clases.Usuario;

public class RegistroActivity extends AppCompatActivity {

    private EditText etNombre, etPassword, etPassword2;
    private Button btnCrear, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etNombre = (EditText)findViewById(R.id.etNombre);
        etPassword = (EditText)findViewById(R.id.etPassword);
        etPassword2 = (EditText)findViewById(R.id.etPassword2);
        btnCrear = (Button)findViewById(R.id.btnCrear);
        btnRegresar = (Button)findViewById(R.id.btnRegresar);

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearUsuario(etNombre.getText().toString(), etPassword.getText().toString(), etPassword2.getText().toString());
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistroActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

    }

    private void crearUsuario(String nombre, String pass, String pass2){
        if(!nombre.equals("") && !pass.equals("") && !pass2.equals("") && pass.equals(pass2)){
            Usuario u = new Usuario(nombre, pass);
            BaseDatos.agregarUsuario(u);
            Toast.makeText(this, "Usuario registrado con exito", Toast.LENGTH_SHORT).show();
        }
        if(!pass.equals(pass2)){
            Toast.makeText(this, "Las contraseñas deben coincidir", Toast.LENGTH_SHORT).show();
        }
        if(nombre.equals("") && pass.equals("") && pass2.equals("") && pass.equals(pass2)){
            Toast.makeText(this, "Porfavor ingrese todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

}
