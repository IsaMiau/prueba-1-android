package com.duoc.isabelazocar_prueba1;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.duoc.isabelazocar_prueba1.Clases.Usuario;

import java.util.ArrayList;

public class AdaptadorActivity extends ArrayAdapter<Usuario> {

    private ArrayList<Usuario> dataSource;

    public AdaptadorActivity(Context context, ArrayList<Usuario> dataSource){
        super(context, R.layout.activity_adaptador, dataSource);
        this.dataSource = dataSource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.activity_adaptador, null);

        TextView tvUsuario = (TextView) item.findViewById(R.id.tvUsuario);
        tvUsuario.setText(dataSource.get(position).getNombre());

        return(item);
    }

}