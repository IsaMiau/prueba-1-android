package com.duoc.isabelazocar_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.duoc.isabelazocar_prueba1.Clases.BaseDatos;
import com.duoc.isabelazocar_prueba1.Clases.Usuario;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    private TextView tvTitulo;
    private EditText etNombre, etPassword;
    private Button btnEntrar, btnRegistrar;
    private ArrayList<Usuario> dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tvTitulo = (TextView)findViewById(R.id.tvTitulo);
        etNombre = (EditText)findViewById(R.id.etNombre);
        etPassword = (EditText)findViewById(R.id.etPassword);
        btnEntrar = (Button)findViewById(R.id.btnEntrar);
        btnRegistrar = (Button)findViewById(R.id.btnRegistrar);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    validarUsuario();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });
    }

    public void validarUsuario() {

        String estado = "";

            if(BaseDatos.obtieneListadoUsuarios().size() == 0){
                estado = "no";
            }

            else{
            for(int x = 0; x < BaseDatos.obtieneListadoUsuarios().size(); x++){
                String nom = BaseDatos.obtieneListadoUsuarios().get(x).getNombre();
                String pass = BaseDatos.obtieneListadoUsuarios().get(x).getClave();

                if(etNombre.getText().toString().equals(nom) && etPassword.getText().toString().equals(pass)) {
                    estado = "si";
                    break;
                }
                else{
                    estado = "no";
                }
            }
            }

            if(estado.equals("no")){
                Toast.makeText(this, "Usuario no registrado", Toast.LENGTH_SHORT).show();
            }
            else{
                Intent i = new Intent(LoginActivity.this, ListadoUsuarioActivity.class);
                startActivity(i);
            }
    }

}


